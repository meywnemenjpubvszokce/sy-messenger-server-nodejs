const ws = require('ws')
const http = require('http')

const PORT = 8000

const server = http.createServer().listen(PORT, () => {
  console.log(`sy-messenger server running on ${PORT}`)
})

server.on('request', (req, res) => {
  let body = ''

  req.on('data', data => {
    body += data
  })

  req.on('end', () => {
    body = JSON.parse(body)

    switch (req.url) {
      case '/room-exists':
        res.writeHead(200, {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        })

        res.end(JSON.stringify({
          exists: roomExists(body.roomId)
        }))

        break
    }
  })
})

const wss = new ws.Server({
  server: server
})

let users = {}
let rooms = {}

wss.on('connection', ws => {
  const user = ws
  const userId = getRandomUserId()
  users[userId] = {
    ws: user,
    roomId: undefined
  }

  ws.send(JSON.stringify({
    action: 'initialData',
    data: {
      userId: userId
    }
  }))

  ws.on('message', message => {
    const jsonedMessage = JSON.parse(message)
    const action = jsonedMessage.action
    const data = jsonedMessage.data

    switch (action) {
      case 'create':
        if (roomExists(data.roomId)) {
          ws.send(JSON.stringify({
            action: 'close',
            data: {
              reason: 'room-exist'
            }
          }))

          ws.close()
        } else {
          rooms[data.roomId] = {
            users: []
          }
        }

        break

      case 'join':
        if (!roomExists(data.roomId)) {
          ws.send(JSON.stringify({
            action: 'close',
            data: {
              reason: 'room-unexist'
            }
          }))

          ws.close()
        } else {
          rooms[data.roomId].users.push(userId)
          users[userId].roomId = data.roomId
          rooms[data.roomId].users
            .filter(roommateId => roommateId !== userId)
            .forEach(roommateId => {
              users[roommateId].ws.send(JSON.stringify({
                action: 'join',
                data: {
                  roommateId: userId
                }
              }))
            })
        }

        break

      case 'chat':
        rooms[data.roomId].users
          .forEach(roommateId => {
            users[roommateId].ws.send(JSON.stringify({
              action: 'chat',
              data: {
                from: userId,
                message: data.message
              }
            }))
          })

        break
    }
  })

  ws.on('close', () => {
    const roomId = users[userId].roomId

    if (roomId !== undefined) {
      rooms[roomId].users = rooms[roomId].users
        .filter(roommateId => roommateId !== userId)

      rooms[roomId].users
        .forEach(roommateId => {
          users[roommateId].ws.send(JSON.stringify({
            action: 'quit',
            data: {
              roommateId: userId
            }
          }))
      })
    }

    delete users[userId]
  })
})

function getRandomUserId() {
  const userIds = Object.keys(users)

  let userId

  while (userIds.indexOf((userId = Math.random().toString(36).substr(2))) !== -1) {}

  return userId
}

function roomExists(roomId) {
  return Object.keys(rooms).indexOf(roomId) !== -1
}

